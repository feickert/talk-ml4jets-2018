# _b_-tagging in ATLAS

[Talk](https://indico.cern.ch/event/745718/contributions/3205085/) given at the [2018 Machine Learning for Jet Physics Workshop](https://indico.cern.ch/event/745718/)

## Authors

Primary Author and Presenter: [Matthew Feickert](http://www.matthewfeickert.com/)

## Acknowledgments

- Ben Nachman and Sergei Gleyzer for the invitation
- The [ATLAS Flavour Tagging Working Group](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FlavourTagging) for helpful discussions and feedback
